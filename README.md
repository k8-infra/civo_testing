# Civo Testing

Using just terraform to create a cluster in Civo and hook up the gitlab agent in a secure(ish) way - WIP

## How to Use

1. Fill in your .env file using the samples from the .env.sample (you will need `direnv`)

2. Run a Terraform apply in the `cluster_bootstrap` folder
3. Run a Terraform apply in the `gl_agent_bootstrap` folder