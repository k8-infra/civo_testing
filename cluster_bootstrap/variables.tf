variable "civo_token" {
  description = "The username for the DB master user"
  type        = string
  sensitive   = true
}
