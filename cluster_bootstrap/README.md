TODO
- Add CI to autogenerate tf docs vs manually

### How to get kubeconfig
```
rm local.kubeconfig
kubectl config unset clusters.my-cluster
terraform output -json | jq -r .kubernetes_cluster_output.value > local.kubeconfig
export PWD_CONFIG=`pwd`
export KUBECONFIG=$PWD_CONFIG/local.kubeconfig:~/.kube/config
kubectl config set-context my-cluster
kubectl get ns;
```

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_civo"></a> [civo](#provider\_civo) | 1.0.16 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [civo_firewall.my-firewall](https://registry.terraform.io/providers/civo/civo/latest/docs/resources/firewall) | resource |
| [civo_firewall_rule.https](https://registry.terraform.io/providers/civo/civo/latest/docs/resources/firewall_rule) | resource |
| [civo_firewall_rule.kubernetes](https://registry.terraform.io/providers/civo/civo/latest/docs/resources/firewall_rule) | resource |
| [civo_kubernetes_cluster.my-cluster](https://registry.terraform.io/providers/civo/civo/latest/docs/resources/kubernetes_cluster) | resource |
| [civo_disk_image.debian](https://registry.terraform.io/providers/civo/civo/latest/docs/data-sources/disk_image) | data source |
| [civo_size.medium](https://registry.terraform.io/providers/civo/civo/latest/docs/data-sources/size) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_civo_token"></a> [civo\_token](#input\_civo\_token) | The username for the DB master user | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_kubernetes_cluster_hostname"></a> [kubernetes\_cluster\_hostname](#output\_kubernetes\_cluster\_hostname) | n/a |
| <a name="output_kubernetes_cluster_output"></a> [kubernetes\_cluster\_output](#output\_kubernetes\_cluster\_output) | n/a |
<!-- END_TF_DOCS -->