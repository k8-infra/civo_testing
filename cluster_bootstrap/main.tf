# Remote Terraform state

# See https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html for details
# terraform init \
#    -backend-config="address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$GITLAB_STATE_NAME" \
#    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$GITLAB_STATE_NAME/lock" \
#    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$GITLAB_STATE_NAME/lock" \
#    -backend-config="username=$GITLAB_USERNAME" \
#    -backend-config="password=$GITLAB_PAT" \
#    -backend-config="lock_method=POST" \
#    -backend-config="unlock_method=DELETE" \
#    -backend-config="retry_wait_min=5"

# Query medium instance size
data "civo_size" "medium" {
  filter {
    key    = "name"
    values = ["g3.medium"]
  }
}

# Query instance disk image
data "civo_disk_image" "debian" {
  filter {
    key    = "name"
    values = ["debian-10"]
  }
}


# Create a firewall
resource "civo_firewall" "my-firewall" {
  name = "my-firewall"
}

# Create a firewall rule
resource "civo_firewall_rule" "kubernetes" {
  firewall_id = civo_firewall.my-firewall.id
  protocol    = "tcp"
  action = "allow"
  start_port  = "6443"
  end_port    = "6443"
  cidr        = ["0.0.0.0/0"]
  direction   = "ingress"
  label       = "kubernetes-api-server"
}

# Create a firewall rule
resource "civo_firewall_rule" "https" {
  firewall_id = civo_firewall.my-firewall.id
  protocol    = "tcp"
  action = "allow"
  start_port  = "443"
  end_port    = "443"
  cidr        = ["0.0.0.0/0"]
  direction   = "ingress"
  label       = "kubernetes-api-server"
}


# Create a cluster
resource "civo_kubernetes_cluster" "my-cluster" {
  name              = "my-cluster"
  applications      = "-Traefik-v2-nodeport"
  pools {
        size = element(data.civo_size.medium.sizes, 0).name
        node_count = 3
    }
  firewall_id       = civo_firewall.my-firewall.id
}
